#[derive(PartialOrd, serde::Deserialize, serde::Serialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ServerMaintainReq {
    /// 服务器id
    #[prost(string, tag = "2")]
    pub server_id: ::prost::alloc::string::String,
    /// 请求服务器状态 1-维护 2-解除维护
    #[prost(int32, tag = "3")]
    pub req_state: i32,
}
#[derive(PartialOrd, serde::Deserialize, serde::Serialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Server {
    /// 服务器ID
    #[prost(string, tag = "1")]
    pub server_id: ::prost::alloc::string::String,
    /// 服务器类型
    #[prost(string, tag = "2")]
    pub server_type: ::prost::alloc::string::String,
    /// 服务器名字
    #[prost(string, tag = "3")]
    pub server_name: ::prost::alloc::string::String,
    /// 服务器IP
    #[prost(string, tag = "4")]
    pub server_ip: ::prost::alloc::string::String,
    /// 客户端端口
    #[prost(int32, tag = "5")]
    pub client_port: i32,
    /// 版本号
    #[prost(int64, tag = "6")]
    pub version: i64,
    /// 是否加载
    #[prost(bool, optional, tag = "7")]
    pub is_launch: ::core::option::Option<bool>,
    /// 是否在维护中
    #[prost(bool, optional, tag = "8")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub maintained: ::core::option::Option<bool>,
    /// 服务根目录
    #[prost(string, optional, tag = "9")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub server_root: ::core::option::Option<::prost::alloc::string::String>,
    /// 是否串行
    #[prost(bool, optional, tag = "10")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub serial: ::core::option::Option<bool>,
    /// 启动权重 越大越晚
    #[prost(int32, optional, tag = "11")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub launch_weight: ::core::option::Option<i32>,
    /// 关闭权重 越大越晚
    #[prost(int32, optional, tag = "12")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shut_weight: ::core::option::Option<i32>,
    /// 负载量
    #[prost(int64, optional, tag = "13")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub load: ::core::option::Option<i64>,
    /// 沉默注册
    #[prost(int32, optional, tag = "14")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub silent: ::core::option::Option<i32>,
}
