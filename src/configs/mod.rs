/*
 *  +----------------------------------------------------------------------
 *  | sophon [ A FAST GAME FRAMEWORK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2023-2029 All rights reserveglobal().
 *  +----------------------------------------------------------------------
 *  | Licensed ( http:www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: jqiris <1920624985@qq.com>
 *  +----------------------------------------------------------------------
 */

use std::collections::HashMap;

pub use define::*;

use crate::treaty::*;

use figment::Figment;
use once_cell::sync::OnceCell;
mod define;

static mut CFG: OnceCell<Config> = OnceCell::new();

fn global() -> &'static Config {
    unsafe { CFG.get().expect("config is not initialized") }
}

fn set_config(cfg: Config) {
    unsafe {
        CFG.set(cfg).unwrap();
    }
}
pub fn init_frame(frame: Figment) {
    let cfg = frame.extract().unwrap();
    set_config(cfg);
}

pub fn get_log_conf() -> define::LogConf {
    global().logger.clone()
}

pub fn get_rpc_conf() -> define::RpcConf {
    global().rpc.clone()
}

pub fn get_discover_conf() -> define::DiscoverConf {
    global().discover.clone()
}

pub fn get_store_conf() -> define::StoreConf {
    global().store.clone()
}

pub fn get_servers_conf() -> HashMap<String, Server> {
    global().servers.clone()
}
