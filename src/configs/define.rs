/*
 *  +----------------------------------------------------------------------
 *  | sophon [ A FAST GAME FRAMEWORK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2023-2029 All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed ( http:www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: jqiris <1920624985@qq.com>
 *  +----------------------------------------------------------------------
 */

use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::treaty::*;

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub logger: LogConf,
    pub discover: DiscoverConf,
    pub rpc: RpcConf,
    pub store: StoreConf,
    pub servers: HashMap<String, Server>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DiscoverConf {
    pub use_type: String,
    pub server_prefix: String,
    pub data_prefix: String,
    pub dial_timeout: u64,
    pub endpoints: Vec<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RpcConf {
    pub use_type: String,
    pub prefix: String,
    pub dial_timeout: u64,
    pub endpoints: Vec<String>,
    pub debug_msg: bool,
    pub auth_user: Option<String>,
    pub auth_passwd: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct StoreConf {
    pub use_type: String,
    pub prefix: String,
    pub dial_timeout: u64,
    pub host: String,
    pub port: u64,
    pub db: u64,
    pub auth_user: Option<String>,
    pub auth_passwd: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LogConf {
    pub log_dir: String,
    pub log_dump: bool,
    pub log_level: String,
    pub log_name: String,
    pub log_runtime: bool,
    pub out_type: String,
    pub std_color: bool,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SslConf {
    pub power_on: bool,
    pub cert_file: String,
    pub key_file: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EncryptConf {
    pub unencrypt: bool,
    pub rsa_private_path: String,
    pub rsa_public_path: String,
    pub aes_key: String,
    pub aes_iv: String,
}
