/*
 *  +----------------------------------------------------------------------
 *  | sophon [ A FAST GAME FRAMEWORK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2023-2029 All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed ( http:www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: jqiris <1920624985@qq.com>
 *  +----------------------------------------------------------------------
 */

use crate::{configs::*, network::*, rpcs::*};

pub struct ServerWebsocket {
    handler: WebsocketHandler,
    ssl_conf: SslConf,
}

impl ServerWebsocket {
    pub fn new(handler: WebsocketHandler, ssl_conf: SslConf) -> Self {
        Self { handler, ssl_conf }
    }
}

impl IServerPlugin for ServerWebsocket {
    fn init(&self, _s: &ServerBase) {
        // info!("init server socket");
    }

    fn after_init(&self, s: &ServerBase) {
        let cfg = s.server_cfg();
        let port = cfg.client_port.clone();
        let handler = self.handler.clone();
        let ws_instance =
            Websocket::new("0.0.0.0".to_string(), port, self.ssl_conf.clone(), handler);
        tokio::spawn(async move {
            ws_instance.run().await;
        });
    }

    fn before_shutdown(&self, _s: &ServerBase) {
        // info!("before shutdown server socket");
    }

    fn shutdown(&self, _s: &ServerBase) {
        // info!("shutdown server socket");
    }
}
