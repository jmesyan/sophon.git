/*
 *  +----------------------------------------------------------------------
 *  | sophon [ A FAST GAME FRAMEWORK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2023-2029 All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed ( http:www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: jqiris <1920624985@qq.com>
 *  +----------------------------------------------------------------------
 */

use crate::{network::HttpHandler, rpcs::*};

pub struct ServerHttp {
    handler: HttpHandler,
}

impl ServerHttp {
    pub fn new(handler: HttpHandler) -> Self {
        Self { handler }
    }
}

impl IServerPlugin for ServerHttp {
    fn init(&self, _s: &ServerBase) {
        // info!("init server http");
    }

    fn after_init(&self, s: &ServerBase) {
        let cfg = s.server_cfg();
        let port = cfg.client_port.clone();
        let handler = self.handler;
        handler("0.0.0.0".to_string(), port);
    }

    fn before_shutdown(&self, _s: &ServerBase) {
        // info!("before shutdown server http");
    }

    fn shutdown(&self, _s: &ServerBase) {
        // info!("shutdown server http");
    }
}
