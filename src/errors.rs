/*
 *  +----------------------------------------------------------------------
 *  | sophon [ A FAST GAME FRAMEWORK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2023-2029 All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed ( http:www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: jqiris <1920624985@qq.com>
 *  +----------------------------------------------------------------------
 */

use thiserror::Error;

#[derive(Error, Debug)]
pub enum DiscoverError {
    #[error("sever not found:{0}")]
    ServerNotFound(String),
    #[error("etcd connnect failed:{0}")]
    EtcdConnectError(String),
    #[error("etcd watch failed:{0}")]
    EtcdWatchError(String),
    #[error("etcd disconnnect failed:{0}")]
    EtcdDisconnectError(String),
    #[error("etcd put failed:{0}")]
    EtcdPutError(String),
    #[error("etcd remove failed:{0}")]
    EtcdRemoveError(String),
    #[error("etcd get failed:{0}")]
    EtcdGetError(String),
}

#[derive(Error, Debug)]
pub enum NatsError {
    #[error("nats server is null")]
    NatsServerNull(),
    #[error("nats get cocder failed:{0}")]
    NatsGetCoderError(String),
    #[error("nats subscribe err:{0}")]
    NatsSubscribeError(String),
    #[error("nats publish err:{0}")]
    NatsPublishError(String),
    #[error("nats encode err:{0}")]
    NatsEncodeError(String),
    #[error("nats decode err:{0}")]
    NatsDecodeError(String),
    #[error("nats request err:{0}")]
    NatsRequestError(String),
    #[error("nats use type err:{0}")]
    NatsUseTypeError(String),
}

#[derive(Error, Debug)]
pub enum EncodeError {
    #[error("json unmarshal err:{0}")]
    JsonUnmarshalError(String),
}

impl From<EncodeError> for NatsError {
    fn from(v: EncodeError) -> Self {
        Self::NatsEncodeError(v.to_string())
    }
}

impl From<anyhow::Error> for NatsError {
    fn from(v: anyhow::Error) -> Self {
        Self::NatsEncodeError(v.to_string())
    }
}
