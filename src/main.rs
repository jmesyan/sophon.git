/*
 *  +----------------------------------------------------------------------
 *  | sophon [ A FAST GAME FRAMEWORK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2023-2029 All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed ( http:www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: jqiris <1920624985@qq.com>
 *  +----------------------------------------------------------------------
 */

use clap::Parser;

pub use app::*;

mod app;

fn main() {
    let app = MicroApp::parse();
    match &app.command {
        Commands::Global(v) => app.global_exec(v),
        Commands::Project(v) => app.project_exec(v),
        Commands::Build(v) => app.build_exec(v),
        Commands::Run(v) => app.run_exec(v),
        Commands::Start(v) => app.start_exec(v),
        Commands::Stop(v) => app.stop_exec(v),
        Commands::Rm(v) => app.rm_exec(v),
        Commands::Rmi(v) => app.rmi_exec(v),
        Commands::Restart(v) => app.restart_exec(v),
        Commands::Clear(v) => app.clear_exec(v),
        Commands::Push(v) => app.push_exec(v),
        Commands::Pull(v) => app.pull_exec(v),
    }
}
