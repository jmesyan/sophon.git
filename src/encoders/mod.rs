/*
 *  +----------------------------------------------------------------------
 *  | sophon [ A FAST GAME FRAMEWORK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2023-2029 All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed ( http:www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: jqiris <1920624985@qq.com>
 *  +----------------------------------------------------------------------
 */

use anyhow::Result;
use once_cell::sync::Lazy;
use prost::Message;
use serde::{de::DeserializeOwned, Serialize};

pub use coder::*;
pub use serializer::*;

mod coder;
mod serializer;

static ENCODER: Lazy<Encoder> = Lazy::new(|| Encoder::new());

pub fn encode_proto<T: Message + Default>(msg: MsgRpc<T>) -> Result<Vec<u8>> {
    ENCODER.encode_proto(msg)
}

pub fn encode_json<T: Serialize + DeserializeOwned + Default>(msg: MsgRpc<T>) -> Result<Vec<u8>> {
    ENCODER.encode_json(msg)
}

pub fn decode_proto<T: Message + Default>(data: Vec<u8>) -> Result<MsgRpc<T>> {
    ENCODER.decode_proto(data)
}

pub fn decode_json<T: Serialize + DeserializeOwned + Default>(data: Vec<u8>) -> Result<MsgRpc<T>> {
    ENCODER.decode_json(data)
}

pub fn decode_header(data: Vec<u8>) -> Result<MsgRpc<Vec<u8>>> {
    ENCODER.decode_header(data)
}

pub fn encode_proto_msg<T: Message + Default>(msg: Option<T>) -> Result<Vec<u8>> {
    ENCODER.encode_proto_msg(msg)
}

pub fn encode_json_msg<T: Serialize + DeserializeOwned>(msg: Option<T>) -> Result<Vec<u8>> {
    ENCODER.encode_json_msg(msg)
}

pub fn decode_proto_msg<T: Message + Default>(data: Vec<u8>) -> Result<T> {
    ENCODER.decode_proto_msg(data)
}

pub fn decode_json_msg<T: Serialize + DeserializeOwned>(data: Vec<u8>) -> Result<T> {
    ENCODER.decode_json_msg(data)
}

pub fn reponse_proto<T: Message + Default>(data: Option<T>) -> Option<Vec<u8>> {
    ENCODER.reponse_proto(data)
}

pub fn reponse_json<T: Serialize + DeserializeOwned + Default>(data: Option<T>) -> Option<Vec<u8>> {
    ENCODER.reponse_json(data)
}
