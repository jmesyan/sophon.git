/*
 *  +----------------------------------------------------------------------
 *  | sophon [ A FAST GAME FRAMEWORK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2023-2029 All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed ( http:www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: jqiris <1920624985@qq.com>
 *  +----------------------------------------------------------------------
 */

// use crate::logger::*;
#[macro_export]
macro_rules! fatal {
    ($($arg:tt)*) => {
        let txt = format!($($arg)*);
        let mut loc = None;
        if print_runtime(LogLevel::FATAL) {
            loc = Some(LogLocation {
                log_file: file!().to_string(),
                log_line: line!(),
            });
        }
        log_macros(LogLevel::FATAL, txt,loc);
    }
}

#[macro_export]
macro_rules! error {
    ($($arg:tt)*) => {
        let txt = format!($($arg)*);
        let mut loc = None;
        if print_runtime(LogLevel::ERROR) {
            loc = Some(LogLocation {
                log_file: file!().to_string(),
                log_line: line!(),
            });
        }
        log_macros(LogLevel::ERROR, txt,loc);
    }
}

#[macro_export]
macro_rules! warn {
    ($($arg:tt)*) => {
        let txt = format!($($arg)*);
        let mut loc = None;
        if print_runtime(LogLevel::WARN) {
            loc = Some(LogLocation {
                log_file: file!().to_string(),
                log_line: line!(),
            });
        }
        log_macros(LogLevel::WARN, txt,loc);
    }
}
#[macro_export]
macro_rules! info {
    ($($arg:tt)*) => {
        let txt = format!($($arg)*);
        let mut loc = None;
        if print_runtime(LogLevel::INFO) {
            loc = Some(LogLocation {
                log_file: file!().to_string(),
                log_line: line!(),
            });
        }
        log_macros(LogLevel::INFO, txt,loc);
    }
}

#[macro_export]
macro_rules! debug {
    ($($arg:tt)*) => {
        let txt = format!($($arg)*);
        let mut loc = None;
        if print_runtime(LogLevel::DEBUG) {
            loc = Some(LogLocation {
                log_file: file!().to_string(),
                log_line: line!(),
            });
        }
        log_macros(LogLevel::DEBUG, txt,loc);
    }
}
#[macro_export]
macro_rules! report {
    ($($arg:tt)*) => {
        let txt = format!($($arg)*);
        let mut loc = None;
        if print_runtime(LogLevel::REPORT) {
            loc = Some(LogLocation {
                log_file: file!().to_string(),
                log_line: line!(),
            });
        }
        log_macros(LogLevel::REPORT, txt,loc);
    }
}
