/*
 *  +----------------------------------------------------------------------
 *  | sophon [ A FAST GAME FRAMEWORK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2023-2029 All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed ( http:www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: jqiris <1920624985@qq.com>
 *  +----------------------------------------------------------------------
 */

use colorful::Color;

#[derive(PartialEq, PartialOrd, Debug, Clone, Copy)]
pub enum LogLevel {
    REPORT = 0,
    //0-上报
    FATAL = 1,
    //1-致命错误
    ERROR = 2,
    //2-错误
    WARN = 3,
    //3-警告
    INFO = 4,
    //4-通知
    DEBUG = 5, //5-调试
}

impl Default for LogLevel {
    fn default() -> Self {
        Self::DEBUG
    }
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub enum OutType {
    OutStd = 0,
    //0-标准输出
    OutFile = 1,
    //1-文件输出
    OutAll = 2, //2-文件和标准都输出
}

pub fn to_level_str(level: &LogLevel) -> String {
    let desc = match level {
        LogLevel::REPORT => "REPORT",
        LogLevel::FATAL => "FATAL",
        LogLevel::ERROR => "ERROR",
        LogLevel::WARN => "WARN",
        LogLevel::INFO => "INFO",
        LogLevel::DEBUG => "DEBUG",
    };
    desc.to_string()
}

pub fn str_to_level(desc: &str) -> LogLevel {
    let level = match desc {
        "report" => LogLevel::REPORT,
        "fatal" => LogLevel::FATAL,
        "error" => LogLevel::ERROR,
        "warn" => LogLevel::WARN,
        "info" => LogLevel::INFO,
        "debug" => LogLevel::DEBUG,
        _ => LogLevel::INFO,
    };
    level
}

pub fn str_to_out_type(desc: &str) -> OutType {
    let outtype = match desc {
        "out_std" => OutType::OutStd,
        "out_file" => OutType::OutFile,
        "out_all" => OutType::OutAll,
        _ => OutType::OutAll,
    };
    outtype
}

pub fn level_to_color(level: LogLevel) -> Color {
    let color = match level {
        LogLevel::REPORT => Color::Red3b,  //红色3a
        LogLevel::FATAL => Color::Magenta, //紫红色
        LogLevel::ERROR => Color::Red,     //红色
        LogLevel::WARN => Color::Yellow,   //黄色
        LogLevel::INFO => Color::Green,    //绿色
        LogLevel::DEBUG => Color::Blue,    //蓝色
    };
    color
}
