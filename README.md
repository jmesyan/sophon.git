# sophon

分布式可扩展可容器化部署服务框架,编译镜像可考虑使用k8s部署

## 基础组件部署
- 创建docker网络-docker network create kf-net
- 运行etcd服务- cd deploy/etcd && docker-compose up -d
- 运行nats服务- cd deploy/nats && docker-compose up -d

## 安装容器部署工具 
- cargo install --git https://gitee.com/jmesyan/sophon.git

## 常用命令
- sophon build x1 ...x2  # 构建docker服务,会根据不同配置文件构建对应服务，例如account,hall等
- sophon push x1 .. x2 # 将本地构建服务上传到远程仓库
- sophon pull x1 .. x2 # 将远程仓库构建服务拉取到本地仓库
- sophon run x1 .. x2 # 创建并运行本地构建服务
- sophon stop x1 .. x2 # 停止本地构建服务
- sophon start x1 .. x2 # 启动已经创建的本地构建服务
- sophon restart x1 .. x2 # 重启已经创建的本地构建服务
- sophon clear x1 .. x2 # 清理已经创建的本地构建服务并清除镜像