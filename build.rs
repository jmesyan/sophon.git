/*
 *  +----------------------------------------------------------------------
 *  | sophon [ A FAST GAME FRAMEWORK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2023-2029 All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed ( http:www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: jqiris <1920624985@qq.com>
 *  +----------------------------------------------------------------------
 */

fn main() {
    let mut config = prost_build::Config::new();
    config.bytes(&["."]);
    config.type_attribute(
        ".",
        "#[derive(PartialOrd,serde::Deserialize, serde::Serialize)]",
    );
    let skip_fields = vec![
        "maintained",
        "server_root",
        "serial",
        "launch_weight",
        "shut_weight",
        "load",
        "silent",
    ];
    for &field in skip_fields.iter() {
        config.field_attribute(field, "#[serde(skip_serializing_if = \"Option::is_none\")]");
    }
    config
        .out_dir("src/treaty")
        .compile_protos(&["pb.proto"], &["."])
        .unwrap();
}
