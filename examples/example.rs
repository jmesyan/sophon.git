/*
 *  +----------------------------------------------------------------------
 *  | sophon [ A FAST GAME FRAMEWORK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2023-2029 All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed ( http:www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: jqiris <1920624985@qq.com>
 *  +----------------------------------------------------------------------
 */

use core::fmt::Error;

use tokio::*;

use figment::{
    providers::{Format, Toml},
    Figment,
};
use sophon::prelude::*;
use sophon::{error, warn};

fn main() {
    let frame = Figment::from(Toml::file("examples/config.toml")).focus("frame");
    init_frame(frame);
    let res = server_init();
    match res {
        Ok(_) => println!("{}", "run ok"),
        Err(err) => println!("{}", err),
    }
}

#[tokio::main]
async fn server_init() -> Result<(), Error> {
    //logger
    let mut ops = Vec::new();
    ops.push(with_log_runtime(false));
    ops.push(with_reporter(|txt: String| {
        println!("it is report:{}", txt);
    }));
    let logger = Logger::new(Some(ops));
    Logger::set_logger(logger);
    //etcd
    let cfg = get_discover_conf();
    init_discoverer(cfg).await;
    //查找当前服务器
    let resp = get_server_list(None).await;
    match resp {
        Some(list) => {
            warn!("server list:{:?}", list);
        }
        None => {
            error!("none server");
        }
    }
    match signal::ctrl_c().await {
        Ok(()) => {}
        Err(err) => {
            error!("Unable to listen for shutdown signal: {}", err);
            // we also shut down in case of error
        }
    }
    Ok(())
}
