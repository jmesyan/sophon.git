/*
 *  +----------------------------------------------------------------------
 *  | sophon [ A FAST GAME FRAMEWORK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2023-2029 All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed ( http:www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: jqiris <1920624985@qq.com>
 *  +----------------------------------------------------------------------
 */

use figment::{
    providers::{Format, Toml},
    Figment,
};
use sophon::info;
use sophon::prelude::*;

#[tokio::main]
async fn main() {
    let frame = Figment::from(Toml::file("sophon/config.toml")).focus("frame");
    init_frame(frame);
    //logger
    let mut ops = Vec::new();
    ops.push(with_log_runtime(false));
    ops.push(with_reporter(|txt: String| {
        println!("it is report:{}", txt);
    }));
    let logger = Logger::new(Some(ops));
    Logger::set_logger(logger);
    let cfg = get_encrypt_conf();
    let ep = Encipher::new(cfg);
    // let word = "welcome to sophon";
    // let eword = ep.rsa_public_encrypt(word.as_bytes());
    // info!("rsa_public_encrypt:{:?}",eword);
    // let data = "SV++bU7O6IW+X9sYzcRXlMXWJEy2ac3YEq4hKHLnQejBbakhRTUm2Sd3VDOWNpF6jBJf7wxGhCvwiCzT0zumTeIrZ1G2sZQM7GGWJ8qNpjpfD20X9X7fsqV/TfcMP4K60cD3DTKtiAwK1RFizEijif+zO2RdQgVFFwp+t+TdzglqQuv2A37liLFXYTAIKeAzz9WOHYjHcF0ORhVz7xSrxElezN9GM5IEH8SD6rM3ptPqBLJPcakdXTMseExS2wioyfzQ/6z70jEYd8yRBc2yXSx+AJ7VySPhwqYEvbyyRFpyFY0+rfYwTYX7JsZ+x99Fl1j2RGGgU++1pGsoJnFE+w==".as_bytes();
    // let resp = ep.rsa_private_decrypt(data).unwrap();
    // info!("rsa_private_decrypt:{}",String::from_utf8(resp).unwrap());
    // let word = "happy to sophon";
    // let eword = ep.rsa_private_encrypt(word.as_bytes());
    // info!("rsa_private_encrypt:{:?}",eword);
    // let data = "SIHD9UPyJXsIOpSfV0DfvkqK0EMln8A6X1ZL3vP/ZZaXdHHGt4b3dPvOreAJCQU3U85gMdQ1N0BlkKFRZTHSy63/dyaV5/n1dX8ZD2hgtmtcmyFenP4/bbZMEBuuWsyub3nwyhPnGVtUiMiZXYpncf7+hhi6yBYz2NLvV1iNV/lPpYohxAhWqLckBKrpg4tDm6DFF+GguVpTKND8oyONGgG4Z5ZlP9jZkHYDTNQ5mzpwHEicoPls4ZqXxIdNeeeLMEPFJRJYPJ2vsh0OO2RTl3uU/yUvLc/J/NM5r2z4NQyBiaE+4WcQe8Lq1HYRseqrEpnqW4g6PJcOY9r03hbEgw==".as_bytes();
    // let resp = ep.rsa_public_decrypt(data).unwrap();
    // info!("rsa_public_decrypt:{}",String::from_utf8(resp).unwrap());
    // let word = "aes encrypt test";
    // let eword = ep.aes_encrypt(word.as_bytes());
    // info!("aes_encrypt:{:?}",eword);
    let data = "KDntUBeiaPCgGvz1yZcRJw==";
    let resp = ep.aes_decrypt(data.as_bytes()).unwrap();
    info!("aes_decrypt:{}", String::from_utf8(resp).unwrap());
}
